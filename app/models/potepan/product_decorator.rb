module Potepan::ProductDecorator
  def relate_products(related_products_count)
    Spree::Product.
      in_taxons(taxons).
      where.not(id: id).
      distinct.
      order(:id).
      limit(related_products_count)
  end
  Spree::Product.prepend self
end
