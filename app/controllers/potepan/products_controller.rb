class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @relation_products = @product.relate_products(Const::RELATED_PRODUCTS_COUNT).includes(master: [:images, :default_price])
  end
end
