RSpec.describe 'Products', type: :model do
  describe 'def relate_products' do
    let(:relating_products_count) { 2 }
    let(:taxon1) { create(:taxon, name: "Shoes") }
    let(:taxon2) { create(:taxon, name: "NIKE") }
    let!(:product1) { create(:product, taxons: [taxon1, taxon2], name: "ランニングシューズ1") }
    let!(:product2) { create(:product, taxons: [taxon1, taxon2], name: "ランニングシューズ2") }
    let!(:product3) { create(:product, taxons: [taxon2], name: "ランニングシューズ3") }
    let!(:product4) { create(:product, taxons: [taxon2], name: "ランニングシューズ4") }

    it '関連商品の数と名前と価格が一致すること' do
      taxon1_related_products = product1.relate_products(relating_products_count)
      expect(taxon1_related_products.count).to eq relating_products_count
      expect(taxon1_related_products[0].name).to eq product2.name
      expect(taxon1_related_products[0].display_price).to eq product2.display_price
      expect(taxon1_related_products[1].name).to eq product3.name
      expect(taxon1_related_products[1].display_price).to eq product3.display_price
    end
  end
end
