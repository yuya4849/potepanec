RSpec.describe "Products", type: :request do
  describe "GETメソッドのshowアクションについて" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, price: "19.91", name: "シューズ1", taxons: [taxon]) }

    describe "showページを表示する" do
      before do
        get potepan_product_path product.id
      end

      it '正常なレスポンスが返ってくること' do
        expect(response).to be_success
      end

      it '200レスポンスが返ってくること' do
        expect(response).to have_http_status(200)
      end

      it '正しい商品の名前や価格を取得すること' do
        expect(response.body).to include product.name
        expect(response.body).to include product.description
        expect(response.body).to include product.display_price.to_s
      end

      describe "正しい関連商品の名前や価格を取得" do
        let!(:taxon1) { create(:taxon) }
        let!(:taxon2) { create(:taxon) }
        let!(:taxon3) { create(:taxon) }
        let!(:product1) { create(:product, price: "19.92", name: "シューズ1", taxons: [taxon1, taxon2]) }
        let!(:product2) { create(:product, price: "19.92", name: "シューズ2", taxons: [taxon1, taxon2]) }
        let!(:unrelate_product1) { create(:product, price: "20.1", name: "シューズ_1", taxons: [taxon3]) }
        let!(:product3) { create(:product, price: "19.93", name: "シューズ3", taxons: [taxon2]) }
        let!(:product4) { create(:product, price: "19.94", name: "シューズ4", taxons: [taxon2]) }
        let!(:product5) { create(:product, price: "19.95", name: "シューズ5", taxons: [taxon2]) }
        let!(:unrelate_product2) { create(:product, price: "20.2", name: "シューズ_2", taxons: [taxon2]) }

        before do
          get potepan_product_path product1.id
        end

        it '正しい関連商品の名前や価格を取得すること' do
          expect(response.body).to include "シューズ2"
          expect(response.body).to include "19.92"
          expect(response.body).to include "シューズ3"
          expect(response.body).to include "19.93"
          expect(response.body).to include "シューズ4"
          expect(response.body).to include "19.94"
          expect(response.body).to include "シューズ5"
          expect(response.body).to include "19.95"
        end

        it '無関連商品の名前や価格が表示されないこと' do
          expect(response.body).not_to include "シューズ_1"
          expect(response.body).not_to include "20.1"
          expect(response.body).not_to include "シューズ_2"
          expect(response.body).not_to include "20.2"
        end
      end
    end
  end
end
