RSpec.describe "Categories", type: :request do
  describe "GETメソッドのshowアクションについて" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path taxon.id
    end

    it '正常なレスポンスが返ってくること' do
      expect(response).to be_success
    end

    it '200レスポンスが返ってくること' do
      expect(response).to have_http_status(200)
    end

    it '正しい商品の名前と価格と画像を取得すること' do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
    end

    it '正しいtaxonの種類を取得すること' do
      expect(response.body).to include taxon.name
    end

    it '正しいtaxonomyの種類を取得すること' do
      expect(response.body).to include taxonomy.name
    end
  end
end
