RSpec.describe "Products", type: :system do
  describe "商品詳細ページの表示" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    it '商品詳細ページの内容を表示すること' do
      visit potepan_product_path product.id
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_content product.name, count: 3
      expect(page).to have_content product.display_price, count: 1
      expect(page).to have_content product.description, count: 1
    end

    describe "トップページへのリンク" do
      before do
        visit potepan_product_path product.id
      end

      def home_click(selector)
        within selector do
          click_on "HOME"
        end
      end

      def top_page_confirmation
        expect(page).to have_title "BIGBAG Store"
        expect(current_path).to eq potepan_path
      end

      it "navbarのロゴをクリックする" do
        click_on "ロゴ"
        top_page_confirmation
      end

      it "navbarのHOMEをクリックする" do
        home_click "li.navbar-nav-home"
        top_page_confirmation
      end

      it "lightSectionのHOMEをクリックする" do
        home_click "li.breadcrumb-home"
        top_page_confirmation
      end
    end
  end

  describe "関連商品を表示" do
    let!(:taxon1) { create(:taxon) }
    let!(:taxon2) { create(:taxon) }
    let!(:taxon3) { create(:taxon) }
    let!(:product1) { create(:product, price: "19.92", name: "シューズ1", taxons: [taxon1, taxon2]) }
    let!(:product2) { create(:product, price: "19.92", name: "シューズ2", taxons: [taxon1, taxon2]) }
    let!(:unrelate_product1) { create(:product, price: "20.1", name: "シューズ_1", taxons: [taxon3]) }
    let!(:product3) { create(:product, price: "19.93", name: "シューズ3", taxons: [taxon2]) }
    let!(:product4) { create(:product, price: "19.94", name: "シューズ4", taxons: [taxon2]) }
    let!(:product5) { create(:product, price: "19.95", name: "シューズ5", taxons: [taxon2]) }
    let!(:unrelate_product2) { create(:product, price: "20.2", name: "シューズ_2", taxons: [taxon2]) }

    before do
      visit potepan_product_path product1.id
    end

    it '関連商品を4つ表示すること' do
      within('#related_products') do
        expect(page).to have_content "関連商品"
        expect(page).to have_content "シューズ2"
        expect(page).to have_content "19.92"
        expect(page).to have_content "シューズ3"
        expect(page).to have_content "19.93"
        expect(page).to have_content "シューズ4"
        expect(page).to have_content "19.94"
        expect(page).to have_content "シューズ5"
        expect(page).to have_content "19.95"
      end
    end

    it '無関連商品を表示しないこと' do
      within('#related_products') do
        expect(page).not_to have_content "シューズ_1"
        expect(page).not_to have_content "20.1"
        expect(page).not_to have_content "シューズ_2"
        expect(page).not_to have_content "20.2"
      end
    end
  end

  describe "商品詳細ページ間を関連商品から移動" do
    let!(:taxon_1) { create(:taxon) }
    let!(:taxon_2) { create(:taxon) }
    let!(:taxon_3) { create(:taxon) }
    let!(:product_1) { create(:product, price: "11.11", name: "バッグ1", taxons: [taxon_1, taxon_2]) }
    let!(:product_2) { create(:product, price: "22.22", name: "バッグ2", taxons: [taxon_2, taxon_3]) }
    let!(:product_3) { create(:product, price: "33.33", name: "バッグ3", taxons: [taxon_3]) }

    it '商品詳細ページ間を関連商品から移動すること' do
      visit potepan_product_path product_1.id

      within('#related_products') do
        expect(page).to have_content "バッグ2"
        expect(page).to have_content "22.22"
        find(".to_relate_product-#{product_2.name}").click
      end
      expect(current_path).to eq potepan_product_path product_2.id

      within('#related_products') do
        expect(page).to have_content "バッグ3"
        expect(page).to have_content "33.33"
        find(".to_relate_product-#{product_3.name}").click
      end
      expect(current_path).to eq potepan_product_path product_3.id
    end
  end
end
