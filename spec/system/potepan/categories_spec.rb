RSpec.describe "Categories", type: :system do
  describe "カテゴリー詳細ページの表示" do
    let(:parent_taxon) { create(:taxon) }
    let(:taxon1) { create(:taxon, name: "Shoes", parent_id: parent_taxon.id) }
    let(:taxon2) { create(:taxon, name: "NIKE", parent_id: parent_taxon.id) }
    let(:taxon3) { create(:taxon, name: "ASICS", parent_id: parent_taxon.id) }
    let!(:product1) { create(:product, taxons: [taxon1], name: "ランニングシューズ") }
    let!(:product2) { create(:product, taxons: [taxon2], name: "ナイキ ランニングシューズ") }
    let!(:product3) { create(:product, taxons: [taxon3], name: "アシックス ランニングシューズ") }

    context '引数にparent_taxonを渡すとき' do
      it '引数にparent_taxonを渡したときにタイトルとカテゴリ名と製品を表示すること' do
        visit potepan_category_path parent_taxon.id
        expect(page).to have_title "#{parent_taxon.name} - BIGBAG Store"

        within('#lightSection') do
          expect(page).to have_content parent_taxon.name, count: 2
        end

        within('#productSection') do
          expect(page).to have_content product1.name
          expect(page).to have_content product1.display_price
          expect(page).to have_content product2.name
          expect(page).to have_content product2.display_price
          expect(page).to have_content product3.name
          expect(page).to have_content product3.display_price
        end
      end
    end

    context '引数にtaxon1を渡すとき' do
      it '引数にtaxon1を渡したときにタイトルとカテゴリ名と製品を表示すること' do
        visit potepan_category_path taxon1.id
        expect(page).to have_title "#{taxon1.name} - BIGBAG Store"

        within('#lightSection') do
          expect(page).to have_content taxon1.name, count: 2
        end

        within('#productSection') do
          expect(page).to have_content product1.name
          expect(page).to have_content product1.display_price
        end
      end
    end

    describe "サイドバーを表示する" do
      it '引数にparent_taxonを渡したときにタイトルとカテゴリ名と製品を表示すること', js: true do
        visit potepan_category_path parent_taxon.id

        within('.side-nav') do
          expect(page).not_to have_selector "a", text: "#{taxon1.name} (#{taxon1.products.count})"
          expect(page).not_to have_selector "a", text: "#{taxon2.name} (#{taxon2.products.count})"
          expect(page).not_to have_selector "a", text: "#{taxon3.name} (#{taxon3.products.count})"
          expect(page).to have_selector "a", text: "#{taxon1.name} (#{taxon1.products.count})", visible: false
          expect(page).to have_selector "a", text: "#{taxon2.name} (#{taxon2.products.count})", visible: false
          expect(page).to have_selector "a", text: "#{taxon3.name} (#{taxon3.products.count})", visible: false
        end
      end
    end

    describe "リンクをクッリクして別のカテゴリーへ移動する" do
      before do
        visit potepan_category_path parent_taxon.id
      end

      it 'Shoesリンクをクリックする' do
        within('.side-nav') do
          click_on "#{taxon1.name} (#{taxon1.products.count})"
        end
        expect(current_path).to eq potepan_category_path taxon1.id
      end

      it 'NIKEリンクをクリックする' do
        within('.side-nav') do
          click_on "#{taxon2.name} (#{taxon2.products.count})"
        end
        expect(current_path).to eq potepan_category_path taxon2.id
      end

      it 'ASICSリンクをクリックする' do
        within('.side-nav') do
          click_on "#{taxon3.name} (#{taxon3.products.count})"
        end
        expect(current_path).to eq potepan_category_path taxon3.id
      end
    end

    describe "商品詳細ページとカテゴリーページ間を移動する" do
      it '商品詳細ページとカテゴリーページへのリンクをクリックすること' do
        visit potepan_category_path parent_taxon.id

        within('#productSection') do
          find(".to_product-#{product1.name}").click
        end
        expect(current_path).to eq potepan_product_path product1.id

        within('.back-to-category') do
          click_link "一覧ページへ戻る"
        end
        expect(current_path).to eq potepan_category_path product1.taxons.first.id
      end
    end
  end
end
