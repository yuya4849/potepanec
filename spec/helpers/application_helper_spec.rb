RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe "全ページのタイトル" do
    context "タイトル名ありの場合" do
      subject { full_title('TOP') }

      it { is_expected.to eq "TOP - BIGBAG Store" }
    end

    context "空文字の場合" do
      subject { full_title('') }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "タイトルの引数が与えられてない場合" do
      subject { full_title }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "タイトルがnilの場合" do
      subject { full_title(nil) }

      it { is_expected.to eq "BIGBAG Store" }
    end
  end
end
